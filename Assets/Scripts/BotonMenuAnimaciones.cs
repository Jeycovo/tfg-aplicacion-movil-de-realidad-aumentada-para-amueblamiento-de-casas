using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class BotonMenuAnimaciones : MonoBehaviour
{
    [SerializeField] UIManager UIManager;
    // Start is called before the first frame update
    void Start()
    {
        var par = false;
        foreach (Transform child in transform)
        {
            if (!par)
            {
                child.DOMoveY(child.position.y + 5, 1.0f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
                par = true;
            }
            else
            {
                child.DOMoveY(child.position.y - 5, 1.0f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
                par = false;
            }
        }
    }

    public void upAndDownAnimation(int type)
    {
        var par = false;
        foreach (Transform child in transform)
        {
            if (!par)
            {
                child.DOMoveX(child.position.x + 20, 0.6f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutCubic);
                par = true;
            }
            else
            {
                child.DOMoveX(child.position.x - 20, 0.6f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutCubic);
                par = false;
            }
        }
        StartCoroutine(waitForEnd(type));
    }

    IEnumerator waitForEnd(int type)
    {
        yield return new WaitForSeconds(1.2f);
        UIManager.menuCalls(type);
        yield return null;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
