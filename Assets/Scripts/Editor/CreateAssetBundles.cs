using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;                  // Nos permite crear el MenuItem
using System.IO;                    // Directory

public class CreateAssetBundles
{
    public static string assetBundleDirectory = "Assets/AssetBundles";

    [MenuItem("Assets/Build AssetBundles")]

    static void BuildAllAssetBundles()
    {

     //* Directorio para guardar los AssetBundles que crearemos
        if (Directory.Exists(assetBundleDirectory))
        {
            Directory.Delete(assetBundleDirectory, true);  // Lo borramos entero, recursivamente.
        }

        Directory.CreateDirectory(assetBundleDirectory);

        //Creamos bundles para Android
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.Android); //NOTA: originalmente era BuildAssetBundleOptions.none
        
        CleanInnecesaryFiles();
        Debug.Log("Android bundle created...");

        RemoveSpacesInFileNames();
        // Creamos nuestro fichero bundlePack.


        AssetDatabase.Refresh();
        Debug.Log("Process complete!");
    }

    // M�todo que quita los espacios innecesarios (los cambia por _)
    static void RemoveSpacesInFileNames()
    {
        foreach (string path in Directory.GetFiles(assetBundleDirectory))
        {
            string oldName = path;
            string newName = path.Replace(' ', '_');
            File.Move(oldName, newName);
        }
    }


    static void CleanInnecesaryFiles()
    {
        foreach (string path in Directory.GetFiles(assetBundleDirectory))
        {

            //Get filename
            string[] files = path.Split('\\');
            string fileName = files[files.Length - 1];
            //Debug.Log("nombre Fichero: " + i +": " + fileName);

            //Delete files we dont need
            if (fileName.Contains("AssetBundles") || fileName.Contains("manifest") || fileName.EndsWith(".meta"))
            {
                File.Delete(path);
            }
            else 
            {
                //append platform to filename
                FileInfo info = new FileInfo(path);
                info.MoveTo(path + "-" + "Android");
            }
        }
    }
}
