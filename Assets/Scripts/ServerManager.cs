using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using UnityEditor;                  

/* Author: Jacobo Placeres Cabrera
 * 
 */
public class ServerManager : MonoBehaviour
{
    [SerializeField] private string jsonURL;                        // Campo donde cargaremos el Json que procesaremos.
    [SerializeField] private ItemButtonManager itemButtonManager;
    [SerializeField] private GameObject buttonsContainer;           // Contenedor de los 
    [SerializeField] private TextAsset localJson;

    /*-> El objeto Item tendr� la misma misma estructura que la del fichero JSON.
     *-> Al declararlo Serializable, podremos visualizarlo en el inspector.
     */

    [SerializeField] private Bundles bundlePack;

    [Serializable] public struct Bundles
    {
        [Serializable]
        public struct Bundle
        {
            public string furnitureName;
            public GameObject model;
            public Material[] materials;
            public RawImage buttonImage;
        }
        public Bundle[] bundles;
    }

    [Serializable] public struct Items
    { 
        [Serializable] public struct Item
        {
            public string Name;                 // Nombre.
            public string PlaneType;            // Tipo de plano
            public string Description;          // Descripci�n.
            public string LongDescription;      // Descripci�n larga.
            public string URLBundleModel;       // Modelo 3D.
            public string URLImageModel;        // Imagen que representa dicho modelo.
            public string URLMaterials;         // Materiales del modelo.
        }

        public Item[] items;
    }

    public Items newItemsCollection = new Items();
    private bool isJson = false;                        //Flag que confirma si ha sido descargado el Json o no.
    public bool online = false;                         //Flag que indica si estamos decargando los modelos o usando los offline.
    /* StartCoroutine(GetJsonData());-> Rutina que nos conseguira el fichero json con la informaci�n de los objetos.
     * OnItemsMenu += CreateButtons;--> Suscribimos el M�todo que creara los botones al pasar al men� de objetos.
     */
    void Start()
    {
        GameManager.instance.OnItemsMenu += CreateButtons;  // suscribimos el M�todo que creara los botones al pasar al men� de objetos.
    }

    /* CreateButtons ---------------------------------------------> Con esta funci�n crearemos los botones, uno por cada item.
     * Instantiate(itemButtonManager, buttonsContainer.transform)-> Creamos un bot�n, su padre ser� el contenedero elegido (content en nuestro caso).
     * OnItemsMenu -= CreateButtons-------------------------------> desuscribimos la funci�n para que se creen los botones una vez sola.
     */
    private void CreateButtons()
    {
        if (isJson)
        {
            int i = 0;
            foreach (var item in newItemsCollection.items)
            {
                ItemButtonManager itemButton;
                itemButton = Instantiate(itemButtonManager, buttonsContainer.transform); // Creamos un bot�n, su padre ser� el contenedor elegido (content en nuestro caso).
                itemButton.ItemName = item.Name;
                itemButton.PlaneType = item.PlaneType;
                itemButton.ItemDescription = item.Description;
                itemButton.ItemLongDescription = item.LongDescription;
 

                Debug.Log("nombre del objeto:" + itemButton.name);
                if (online)
                {
                    itemButton.URLBundleModel = item.URLBundleModel;
                    itemButton.URLMaterialModel = item.URLMaterials;
                    itemButton.URLBundleImage = item.URLImageModel;
                }
                else
                {
                    itemButton.ItemModel = bundlePack.bundles[i].model;
                    itemButton.Materials = bundlePack.bundles[i].materials;
                    itemButton.ImageButtonTexture = bundlePack.bundles[i].buttonImage.texture;
                    i++;
                }
            }
            GameManager.instance.OnItemsMenu -= CreateButtons;
        }
        else
        {
            // Si no lo ha conseguido descargar, que use el local.
            if (online)
            {
                StartCoroutine(GetJsonData()); // Rutina que nos conseguira el fichero json con la informaci�n de los objetos.
            }
            else
            {
                GetLocalJsonData();
                CreateButtons();
            }
        }
    }

    /* GetJsonData()-----------------------> Funci�n que emplearemos para obtener el JSON con la informaci�n de los modelos.
     *-> Una corutina es una funci�n que tiene la hablidad de pausar su ejecuci�n y 
     *  devolver el control a Unity para continuar donde lo dejo el siguiente frame.
     *-> Una corutina en unity, esfunci�n con tipo de retorno IEnumerator con alguna 
     *  instrucci�n de retorno yield en alg�n lugar de su cuerpo.
     * IEnumerator-------------------------> Enumerador, que admite una iteraci�n simple en una colecci�n no gen�rica.
     * yield return------------------------> Instrucci�n empleada en un iterador para proporcionar 
     *                                       el siguiente valor de una secuencia al iterar la secuencia.
     *-------------------------------------> yield return se diferencia de un return normal en que devuelve un valor de la funci�n, pero
     *                                       no la termina en dicho instante.
     * UnityWebRequest---------------------> Provee m�todos para la comunicaci�n entre servidores web. Dicho de otra forma, 
     *                                       maneja el flujo HTTP de informaci�n entre servidores webs. Para descargar y subir datos, 
     *                                       se emplean el DownloadHandler y el UploadHandler Respectivamente. 
     * UnityWebRequest.Get()---------------> Crea un UnityWebRequest para HTTP GET, devuelve un objeto que recupera informaci�n desde 
     *                                       una URL.
     * serverRequest.SendWebRequest()------> Cuando se llama a est� m�todo, el objeto UnityWebRequest comienza a comunicarse con el server remoto. 
     *                                       Despu�s el UnityWebRequest ejecuta la resoluci�n DNS de ser necesario. A continuaci�n, realizar� una
     *                                       solicitud HTTP al server remoto que la URL indique y finalmente, procesar� la respuesta del servidor. 
     *-------------------------------------> Una vez SendWebRequest() es llamado, ya no se pueden cambiar las propiedades del UnityWebRequest. 
     *-------------------------------------> Devuelve un objeto WebRequestAsyncOperation, que si es Yield dentro de una corutina, esta se 
     *                                       se pausara hasta que el UnityWebRequest encontrase un error de sistema o terminase de comunicarse.
     * ServerRequest.result----------------> El resultado del UnityWebRequest.
     * UnityWebRequest.Result.Success------> Uno de los posibles resultados del UnityWebRequest, definido dentro del enumerador Result. 
     *                                       Su significado es que la solicitud tuvo �xito.
     * JsonUtility.FromJson<>()------------> Esta funci�n crea un objeto a partir de su representaci�n JSON.
     * serverRequest.downloadHandler-------> Maneja y procesa los datos HTTP mandados desde el servidor remoto.
     *-------------------------------------> Adherido a un UnityWebRequest, define como se gestiona el tratamiento de los datos recibidos.
     *-------------------------------------> Hay varios tipos de DownloadHandler, este provee con buffering b�sico.
     * serverRequest.downloadHandler.text--> Propiedad de solo lectura que devuelve los bytes de los datos, interpretados como una c�dena de texto UTF8.
     */
    IEnumerator GetJsonData()
    {
        UnityWebRequest serverRequest = UnityWebRequest.Get(jsonURL);
        yield return serverRequest.SendWebRequest();                    
        if (serverRequest.result == UnityWebRequest.Result.Success)     
        {
            newItemsCollection = JsonUtility.FromJson<Items>(serverRequest.downloadHandler.text); //Asignamos la informaci�n a nuestro objeto
            isJson = true;
        }
        else
        {
            isJson = false;
            Debug.Log("Error...Server Request failed for obtaining JSON :: " + serverRequest.result);
        }
    }
    // DONW: Meter este metodo en ItemButtonManager, y realizar la descarga de todo a la vez.
    
    private void GetLocalJsonData()
    {
        newItemsCollection = JsonUtility.FromJson<Items>(localJson.text);
        isJson = true;
    }
}
