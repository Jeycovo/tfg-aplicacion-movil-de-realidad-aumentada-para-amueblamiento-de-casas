using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI; //Definimos el namespace del canvas de Unity.
//Atributos del DataManager que ser�n asociados a los botones

public class ItemButtonManager : MonoBehaviour
{
    [SerializeField] private FurnitureItem furnitureItem;
    private FurnitureSpace furnitureSpace;
    private ARInteractionsManager interactionsManager;  // Clase que empleamos para gestionar las interacciones de RA.

    //Atributos que ser�n asociados a los botones cuando se creen
    private string urlBundleModel;              // URL del Bundle con el modelo.
    private string urlMaterial;                 // URL del Bundle con el/los material/materiales.
    private string urlBundleImage;              // URL de

    //- Partes del boton
    private TextMeshProUGUI titleButton;        //
    private RawImage imageButton;        // Imagen del mueble que se emplea para el bot�n.
    private TextMeshProUGUI descriptionButton;  //
    private string itemDescription;    // Descripci�n abreviada del mueble, se mostr�ra en el bot�n.


    //--Atributos que vamos a meter en el objeto ItemFurniture. Algunos los usamos para el dise�o del boton.
    private string itemName;                // Nombre del mueble, se emplear� como t�tulo del bot�n.
    private string planeType;
    private GameObject itemModel;               // Modelo 3D del mueble, se instanciara/duplicar� cada vez que pulse el bot�n.
    private string itemLongDescription;     // Descripci�n detallada del mueble.
    private Material[] materials;
    
    // Getters && Setters
    
    public GameObject ItemModel             { set => itemModel = value;}
    public Material[]  Materials            { set => materials = value;}
    public Texture ImageButtonTexture       { set => imageButton.texture = value;}
    
    public string PlaneType
    {
        set
        {
            planeType = value;
        }
    }

    public string ItemName
    {
        set
        {
            itemName = value;
            titleButton.text = value;
        }
    }
    public string ItemDescription
    {
        set
        {
            itemDescription = value;
            descriptionButton.text = itemDescription;
        }
    }

    public string ItemLongDescription { set => itemLongDescription = value; }

    public string URLBundleModel {
        set
        {
            urlBundleModel = value;
            StartCoroutine(DownLoadAssetBundle(urlBundleModel, 0));
        }
    }
    public string URLMaterialModel {
        set
        {
            urlMaterial = value;
            StartCoroutine(DownLoadAssetBundle(urlMaterial, 1));
        }
    }
    public string URLBundleImage {
        set
        {
            urlBundleImage = value;
            StartCoroutine(DownloadtBundleImage());
        }
    }

    // Metodos para settear off-line

    void Awake()
    {
        interactionsManager = FindObjectOfType<ARInteractionsManager>();                    // Buscamos el objeto ARInteractionsManager.
        titleButton = transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
        imageButton = transform.GetChild(1).GetComponent<RawImage>();
        descriptionButton = transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>();
        furnitureSpace = FindObjectOfType<FurnitureSpace>();
    }

    void Start()
    {
        var button = GetComponent<Button>();                                                // Obtenemos el bot�n asociado a este script.
        button.onClick.AddListener(Create3DModel);                                          // Al clicar en el bot�n, llamamos al m�todo Create3DModel. 
        button.onClick.AddListener(GameManager.instance.ARPosition);                        // Al clicar en el bot�n, se activa el evento ARPosition (cambiamos de pantalla).
    }

    /* M�todo que empleamos para crear el modelo.
     * Cada vez que es llamado, ejecuta una corutina con el DownLoadAssetBundle para descargar el modelo correspondiente.
    */

    private void Create3DModel()
    {
        if (itemModel && furnitureSpace)
        {
            var furniture = Instantiate(furnitureItem, furnitureSpace.transform);
            furniture.ItemName = itemName;
            furniture.PlaneType = planeType;
            furniture.ItemLongDescription = itemLongDescription;
            var newItemModel = Instantiate(itemModel, furniture.transform); //Instanciamos el modelo 3D y lo volvemos hijo del ojbeto de furniture que vamos a crear tambi�n.
            furniture.Item3DModel = newItemModel;
            furniture.Textures = materials;
            interactionsManager.ItemSelected = furniture;
            interactionsManager.ItemPositioningOn();            
        } 
        else
        {
            Debug.Log("Error. Modelo del mueble no descargado...");
        }
    }
   
    /* GetBundleImage()-----------> Obtenemos la imagen que reperesentara el item.
     * DownloadHandlerTexture()---> Una subclase de DownloadHandler especializada en descargas de imagenes para ser usadas como objetos Texture.
     *----------------------------> Este m�todo almacena la informaci�n recibida en un objeto Texture pre-asignado (allocated) como un objeto Texture de Unity.
     */
    IEnumerator DownloadtBundleImage()
    {
        UnityWebRequest serverRequest = UnityWebRequest.Get(urlBundleImage);
        serverRequest.downloadHandler = new DownloadHandlerTexture();
        yield return serverRequest.SendWebRequest();
        if (serverRequest.result == UnityWebRequest.Result.Success)
        {
            imageButton.texture = ((DownloadHandlerTexture)serverRequest.downloadHandler).texture;
        }
        else
        {
            Debug.Log("Error...Server Request failed for obtaining BundleImage.");
            Debug.Log("Tipo de Error: " + serverRequest.result);
        }
    }

    /* M�todo para descargar e instanciar (como GameObject) el modelo 3D del mueble correspondiente al bot�n(como GameObject). 
     * Tiene dos modos:
     *      0 -> Modelo del Mueble.
     *      1 -> Textura o Texturas.
     *      
     * UnityWebRequestAssetBundle.GetAssetBundle(urlAssetBundle)------------> Creamos un helper, para descargar mediante UnityWebRequest, bundles.
     * yield return serverRequest.SendWebRequest----------------------------> Nos comunicamos con el server remoto para realizar la descarga. Al hacer un yield con el
     *                                                                         resultado, la corutina esperara a que la comunicaci�n con el servidor remoto termine o falle.
     * assetBundle = DownloadHandlerAssetBundle.GetContent(serverRequest)---> Devuelve el AssetBundle (modelo o materiales) descargado o null, si dio error.
     * -------------------------------------------------------------------------> C�mo par�metro recibe un UnityWebRequest con un DownloadHandlerAssetBundle asociado.
     * 
     * -- 0: M�delo del mueble --
     * Instantiate(----------------------------------------------> Clonamos el objeto pasado por par�metro. Este metodo devuelve el clon.
     *          assetBundle.LoadAsset(---------------------------> Carga el asset con el nombre determinado, desde el bundle.
     *                   assetBundle.GetAllAssetNames()[0]) -----> Devuelve todos los nombres de los assets que se encuentran en el AssetBundle.
     *          as GameObject);----------------------------------> Queremos que el clon sea instanciado como un GameObject.
     * itemModel.AddComponent<ItemProperties>();-----------------> A�adimos un script con las propiedades del objeto
     * ItemProperties.ItemLongDescription = itemLongDescription;-> A�adimos la descripci�n detallada, que obtuvimos al crear el bot�n en el script ServerManager.
     * ItemProperties.ItemName = itemName;-----------------------> A�adimos el nombre del objeto, que obtuvimos tambi�n, al crear el bot�n en el script ServerManager.
     * interactionsManager.Item3DModel = itemModel;--------------> Le pasamos el modelo a ARInteractionsManager, ya que hemos instanciado el modelo del mueble, debemos manejarlo ahora.
     * StartCoroutine(DownLoadAssetBundle(urlMaterial, 1));------> Ejecutamos otra corutina para descargar los materiales, usando el mismo m�todo.
     * 
     *-- 1: pack de texturas --
     * var a = assetBundle.LoadAllAssets();------------------------> Carga todos los assets contenidos en el Bundle de assets, admite tipo.s
     * Material[] bundleTexture = new Material[a.Length];----------> Creamos un vector de Material con longitud igual al n�mero de texturas en el AssetBundle.
     * bundleTexture[i] = a[i] as Material;------------------------> lo guardamos como un material, debemos especificar el tipo.
     * interactionsManager.Item3DModel.GetComponent<ItemProperties>().Textures = bundleTexture;----> Ahora que hemos descargado y desempaquetado las texturas, se lo asignamos al ItemProperties.
     *
     *-- Final --
     *assetBundle.Unload(false);----------------------------------> Liberamos el espacio del AssetBundle descargado. Le hemos pasado como par�metro false, por lo tanto, los ficheros de datos 
     *                                                              comprimidos de dentro del bundle son liberados, pero las instancias de objetos cargados desde este bundle se mantienen intactos.
     *                                                              Despu�s de llamar a UnloadAsync sobre un AssetBundle, no podr�s cargar m�s objetos desde ese bundle y otras operaciones hechas en 
     *                                                              el bundle dispararan un error InvalidOperationException. Despues de llamar a UnloadAsync, no podr�s cargar m�s objetos desde ese
     *                                                              bundle y otras.
     */
    IEnumerator DownLoadAssetBundle(string urlAssetBundle, int type)
    {
        UnityWebRequest serverRequest = UnityWebRequestAssetBundle.GetAssetBundle(urlAssetBundle);
        yield return serverRequest.SendWebRequest();
        if (serverRequest.result == UnityWebRequest.Result.Success)
        {
            AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(serverRequest);
            if (assetBundle != null)
            {
                if (type == 0) // Bundle con el modelo del mueble.
                {
                    itemModel = assetBundle.LoadAsset(assetBundle.GetAllAssetNames()[0]) as GameObject;
                    //interactionsManager.ModelFunctions.Item3DModel = itemModel; // �Quitar?
                }
                else if (type == 1) // Bundle con los materiales del modelo.
                {
                    var a = assetBundle.LoadAllAssets();
                    Material[] bundleTexture = new Material[a.Length];
                    Debug.Log("bundleTexture Vector:" + bundleTexture);
                    for(int i = 0; i < a.Length; i++)
                    {
                        bundleTexture[i] = a[i] as Material;
                        Debug.Log("BundleTexture, Elemento " + i + ": " + bundleTexture[i]);
                    }
                    materials = bundleTexture; 
                }
                assetBundle.Unload(false);
            }
            else
            {
                Debug.Log("Not a valid Asset Bundle");
            }
        }
        else
        {
            Debug.Log("Not a valid Asset Bundle");
        }
    }
}
