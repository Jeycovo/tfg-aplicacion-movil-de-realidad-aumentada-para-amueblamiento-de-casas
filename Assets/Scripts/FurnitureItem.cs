using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureItem : MonoBehaviour
{
    // TODO: Volverlo todo privado para la versi�n final.
    // -- P�rametros 
    public string itemName;               // Nombre del mueble, aparecera en la parte superior del boton.
    public string planeType;
    public GameObject item3DModel;            // Modelo 3D del objeto
    public string itemLongDescription;    // Descripci�n larga del mueble, explica m�s detalles sobre el mismo.
    public Vector3 originalScale;          // Guardamos la escala con la que se crea el mueble, con el prop�sito de devolver el mueble a su tama�o original.
    public Material[] textures;               // Vector donde almacenamos los distintos materiales de los modelos 3D de los muebles.
    public int indexTexture = 0;           // �ndice del material que estamos empleando actualmente.
     

    // -- Getters & Setters
    public GameObject Item3DModel
    {
        set
        {
            item3DModel = value;
            originalScale = item3DModel.transform.localScale;
        }
        get => item3DModel;
    }

    public string ItemLongDescription { set => itemLongDescription = value; get => itemLongDescription; }
    public string ItemName { set => itemName = value; get => itemName; }
    public string PlaneType { set => planeType = value; get => planeType; }
    public Material [] Textures { set => textures = value; }
    public Vector3 OriginalScale {set => originalScale = value; }

    //-------
    public void Start()
    {
    }

    // Borramos el objeto.
    public void DeleteItem()
    {
        Destroy(this.gameObject);            // Destruimos este objeto
    }

    public void ResetScale()
    {
        this.item3DModel.transform.localScale = this.originalScale;
    }

    // ** Cambiar el material de los items **
    public void ChangeMaterial()
    {
        if (textures != null && textures.Length > 1)
        {
            indexTexture = (indexTexture + 1) % textures.Length;
            var newMaterial = textures[indexTexture];
            Debug.Log("newMaterial: " + newMaterial);
            if (newMaterial)
            {
                item3DModel.transform.GetChild(0).GetComponent<MeshRenderer>().material = newMaterial;
            }
        }
        else
        {
            Debug.Log("Este modelo no tiene texturas cargadas.");
        }
    }
}
