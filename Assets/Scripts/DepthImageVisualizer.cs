using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class DepthImageVisualizer : MonoBehaviour
{
    public ARCameraManager CameraManager
    {
        get => _cameraManager;
        set => _cameraManager = value;
    }

    public AROcclusionManager OcclusionManager
    {
        get => _occlusionManager;
        set => _occlusionManager = value;
    }

    public RawImage RawImage
    {
        get => _rawImage;
        set => _rawImage = value;
    }

    [SerializeField]
    [Tooltip("The ARCameraManager which will produce camera frame events.")]
    private ARCameraManager _cameraManager;

    [SerializeField]
    [Tooltip("The AROcclusionManager which will produce depth textures.")]
    private AROcclusionManager _occlusionManager;

    [SerializeField]
    [Tooltip("The UI RawImage used to display the image on screen.")]
    private RawImage _rawImage;


    void Update()
    {
        if (OcclusionManager.TryAcquireEnvironmentDepthCpuImage(out XRCpuImage image) && (RawImage.enabled))
        {
            using (image)
            {
                // Use the texture.
                UpdateRawImage2(_rawImage, image);
            }
        }
    }

    private static void UpdateRawImage2(RawImage rawImage, XRCpuImage cpuImage)
    {
        Debug.Assert(rawImage != null, "no raw image");
        Debug.Assert(cpuImage != null, "no cpu image");

        var texture = new Texture2D(cpuImage.width, cpuImage.height, cpuImage.format.AsTextureFormat(), false);
        rawImage.texture = texture;
        //TextureFormat.R16
        var conversionParams = new XRCpuImage.ConversionParams(cpuImage, cpuImage.format.AsTextureFormat(), XRCpuImage.Transformation.MirrorY);
        var rawTextureData = texture.GetRawTextureData<byte>();

        Debug.Assert(rawTextureData.Length == cpuImage.GetConvertedDataSize(conversionParams.outputDimensions, conversionParams.outputFormat),
            "The Texture2D is not the same size as the converted data.");

        cpuImage.Convert(conversionParams, rawTextureData);
        texture.Apply();
        cpuImage.Dispose();
        
        var textureAspectRatio = (float)texture.width / (float)texture.height;

        float minDimension = Screen.width;
        var maxDimension = Mathf.Round(minDimension * textureAspectRatio);
        Vector2 rectSize  = new Vector2(maxDimension, minDimension);
        rawImage.rectTransform.sizeDelta = rectSize;
        rawImage.transform.rotation = Quaternion.Euler(0, 0, 90);

    }

    private static void UpdateRawImage(RawImage rawImage, XRCpuImage cpuImage)
    {
        Debug.Assert(rawImage != null, "no raw image");
        Debug.Assert(cpuImage != null, "no cpu image");

        // Get the texture associated with the UI.RawImage that we wish to display on screen.
        var texture = rawImage.texture as Texture2D;

        // If the texture hasn't yet been created, or if its dimensions have changed, (re)create the texture.
        // Note: Although texture dimensions do not normally change frame-to-frame, they can change in response to
        //    a change in the camera resolution (for camera images) or changes to the quality of the human depth
        //    and human stencil buffers.
        if (texture == null || texture.width != cpuImage.width || texture.height != cpuImage.height)
        {
            texture = new Texture2D(cpuImage.width, cpuImage.height, cpuImage.format.AsTextureFormat(), false);
            rawImage.texture = texture;
        }

        // For display, we need to mirror about the vertical access.
        var conversionParams = new XRCpuImage.ConversionParams(cpuImage, cpuImage.format.AsTextureFormat(), XRCpuImage.Transformation.MirrorY);

        //Debug.Log("Texture format: " + cpuImage.format.AsTextureFormat()); -> RFloat

        // Get the Texture2D's underlying pixel buffer.
        var rawTextureData = texture.GetRawTextureData<byte>();

        // Make sure the destination buffer is large enough to hold the converted data (they should be the same size)
        Debug.Assert(rawTextureData.Length == cpuImage.GetConvertedDataSize(conversionParams.outputDimensions, conversionParams.outputFormat),
            "The Texture2D is not the same size as the converted data.");

        // Perform the conversion.
        cpuImage.Convert(conversionParams, rawTextureData);

        // "Apply" the new pixel data to the Texture2D.
        texture.Apply();
        // Dispose of the XRCpuImage
        cpuImage.Dispose();

        // Get the aspect ratio for the current texture.
        var textureAspectRatio = (float)texture.width / (float)texture.height;

        //** Ajustamos el tama�o del rawImage para que acomode la textura manteniendo la escala. **
        const float minDimension = 480.0f;
        var maxDimension = Mathf.Round(minDimension * textureAspectRatio);
        Vector2 rectSize;

        rectSize = new Vector2(maxDimension, minDimension);

        //* Actualizamos las dimensiones de raw image y las propiedades de su material
        rawImage.rectTransform.sizeDelta = rectSize;
        
        // Rotation
        switch (Screen.orientation)
        {
            case ScreenOrientation.LandscapeRight:
                rawImage.transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case ScreenOrientation.Portrait:
                rawImage.transform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case ScreenOrientation.LandscapeLeft:
                rawImage.transform.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case ScreenOrientation.PortraitUpsideDown:
                rawImage.transform.rotation = Quaternion.Euler(0, 0, 270);
                break;
            default:
                break;
        }
    }
}

