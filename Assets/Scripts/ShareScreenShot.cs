using System.Collections;
using System.Collections.Generic;
using System.IO;		//Libreria que nos dar� las funciones necesarias para acceder a los ficheros
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ShareScreenShot : MonoBehaviour
{
	[SerializeField] private GameObject mainMenuCanvas;	// Atributo para activar/desactivar el canvas.
	[SerializeField] private GameObject helperButton;	// Atributo para activar/desactivar el canvas.

	private ARPointCloudManager aRPointCloudManager;	// Atributo para activar/desactivar la nube de puntos.

    // Start is called before the first frame update
    void Start()
    {
		aRPointCloudManager = FindObjectOfType<ARPointCloudManager>(); //Referencio la nube de puntos en el start.
    }

    public void TakeScreenShot()
    {
		TurnOnOffARContents();
		StartCoroutine(TakeScreenshotAndShare()); // A coroutine is a function that can suspend its execution (yield) until the given YieldInstruction finishes.
	}
	
	private void TurnOnOffARContents()
    {
		/*
		var points = aRPointCloudManager.trackables;			  // Desactivo cada punto que se encuentra en la nube de puntos.
		foreach (var point in points)
        {
			point.gameObject.SetActive(!point.gameObject.activeSelf); // Desactivamos/Activamos puntos
        }
		*/
		mainMenuCanvas.SetActive(!mainMenuCanvas.activeSelf); // Si esta activado, se desactivara, si esta desactivado se activara.
		helperButton.SetActive(!helperButton.activeSelf); // Si esta activado, se desactivara, si esta desactivado se activara.
		
    }

	private IEnumerator TakeScreenshotAndShare()
	{
		yield return new WaitForEndOfFrame();

		Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		ss.Apply();

		string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
		File.WriteAllBytes(filePath, ss.EncodeToPNG());

		// To avoid memory leaks
		Destroy(ss);

		new NativeShare().AddFile(filePath)
			//.SetSubject("Subject goes here").SetText("Hello world!").SetUrl("https://github.com/yasirkula/UnityNativeShare") //Nos permite compartir un mensaje junto a nuestra foto
			.SetSubject("ARFuniture_photo").SetText("Compartido desde ARFuniture")
			.SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
			.Share();

		TurnOnOffARContents();
		// Share on WhatsApp only, if installed (Android only)
		//if( NativeShare.TargetExists( "com.whatsapp" ) )
		//	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();
	}
}
