using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; //Referenciamos el namespace

public class GameManager : MonoBehaviour
{
    // Eventos/Estados de nuestra app
    public event Action OnStartMenu;
    public event Action OnMainMenu;
    public event Action OnItemsMenu;
    public event Action OnARPosition;
    public event Action OnPlaneCalibration;

    // Para crear y llamar a los eventos usamos el patron singleton:
    // Singleton:
    // -> Esta restringido a una sola instancia.
    // -> Es globalmente accesible.

    public static GameManager instance;
    // El m�todo Awake se ejecuta antes que start, se le llama justo despu�s de que el objeto haya sido instanciado.
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        performanceSetting();
    }

    private void performanceSetting()
    {
        Application.targetFrameRate = 30; //Reducimos los frames
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // OnMainMenu?.Invoke(); equivale a ->
    /*
     if (OnMainMenu != null){
        OnMainMenu(); 
    }
     */

    // Disparadores: Llamar a estos m�todos activa los distintos eventos

    public void StartMenu()
    {
        OnStartMenu?.Invoke();
        Debug.Log("Start Menu Activado");
    }

    public void MainMenu()
    {
        OnMainMenu?.Invoke(); 
        Debug.Log("Main Menu Activado");
    }
    
    public void ItemsMenu()
    {
        OnItemsMenu?.Invoke();
        Debug.Log("Items Menu Activado");
    }

    public void ARPosition()
    {
        OnARPosition?.Invoke();
        Debug.Log("AR Position Activado");
    }

    public void PlaneCalibration()
    {
        OnPlaneCalibration?.Invoke();
        Debug.Log("Plane Calibration Activado");
    }

    // Cerrar la aplicaci�n
    public void CloseApp()
    {
        Debug.Log("Saliendo de la app.");

        // Si estamos en un dispositivo Android.
        if (Application.platform == RuntimePlatform.Android) 
        { 
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer")
                .GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
        }
        else
        {
            Application.Quit();
        }
    }

}
