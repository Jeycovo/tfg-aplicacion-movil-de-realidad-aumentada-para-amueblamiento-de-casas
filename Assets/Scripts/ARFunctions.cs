using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using DG.Tweening;

namespace ArFunctions
{
    // Clase con funciones relacionas con la gesti�n del Ray-Cast.
    public class RayCastManager
    {

        //-- Par�metros
        /*  ARRaycastManager -> Gestiona un XRRaycastSubsystem, a trav�s del c�al da funcionalidad raycast en ARFoundation.
         * Este componente se emplea para realizar raycast contra trackables (por ejempmlo, caracter�sticas
         * detectadas en el medio f�sico) que no tienen contraparte en el medio f�sico.
         *  */
        private ARRaycastManager castManager;
        private List<ARRaycastHit> hits = new List<ARRaycastHit>();     // Lista de hits (planos con los que ha chocado el rayCast)  
        private ARRaycastHit bestHit;

        //-- Setters & Getters
        // Devuelve la lista de hits (planos con los que ha chocado el raycast y cumplen los p�rametros establecidos)
        public List<ARRaycastHit> getHits { get => hits; }
        public ARRaycastHit BestHit { get => bestHit; }
        public ARPlane BestPlane { get => BestPlane; }

        //-- Constructor
        public RayCastManager(ARRaycastManager manager)
        {
            castManager = manager;      // Manager.
        }

        // Funci�n que castea el rayo para detectar planos.
        public bool CastRay(Vector2 Position, string type)
        {
            bool hit = castManager.Raycast(Position, hits, TrackableType.Planes);
            if (hit == false) return hit;       
            switch (type) 
            {
                case "wall":
                    return GetPlaneHit(PlaneAlignment.Vertical);
                case "ceiling":
                    return GetPlaneHit(PlaneAlignment.HorizontalDown);
                case "floor":
                    return GetPlaneHit(PlaneAlignment.HorizontalUp);
                default:
                    this.bestHit = hits[hits.Count - 1];
                    return true;
            }
        }

        // la lista hit esta ordenado por cercania (de m�s cerca a menos).
        // La recorreremos por el final (el elemento m�s lejano)
        /* 
         * 1) Recorre todos los hits de m�s cercano a lejano.
         * 2) Si es un plano y tiene el tipo de alineaci�n correcta sustituye el bestHit.
         * 3) Ahora, si al final si no nos hemos encontrado con ningun plano que no sea del 
        *      tipo que buscamos tendremos el plano m�s lejano que hemos detectado.
        */
        private bool GetPlaneHit(PlaneAlignment planeType)
        {
            int correctPointsFounded = 0;
            foreach (ARRaycastHit hit in hits)
            {
                if (hit.trackable is ARPlane plane)
                {
                    if (plane.alignment == planeType)
                    {
                        bestHit = hit;
                        correctPointsFounded++;
                        return true; 
                    }
                    else
                    {
                        if ( correctPointsFounded > 0) { return true; }
                        else { return false; }
                    }
                }
            }
            if (correctPointsFounded > 0)  return true; 
            else { return false; }
        }
    }

 // *********************************************************************************************************************

    public class touch 
    {
        private Camera aRCamera;   // La c�mara del movil

        public touch (Camera aRCamera){
            this.aRCamera = aRCamera;
        }

        /* isTapOver3DModel :: M�todo para detectar si el toque en pantalla (touch) est� tocando un modelo 3D (mueble) que ya hallamos colocado.
         * Camera.ScreenPointToRay(Vector3 pos);-------------> Devuelve un ray que parte desde la c�mara a trav�s de un punto de la pantalla.
         *---------------------------------------------------> El rayo resultante est� en el espacio global, comenzando en el plano cercano de la c�mara y 
         *                                                     pasando por las coordenadas (x,y) desde la posici�n en la pantalla (�a position.z se ignora).
         *---------------------------------------------------> El espacio de la pantalla se define en p�xeles. La parte inferior izquierda de la pantalla es (0,0);
         *                                                     la parte superior derecha es (pixelWidth -1,pixelHeight -1).
         * Physics.------------------------------------------> Propiedades f�sicas globales y m�todos de ayuda.
         *         Raycast(Ray ray, out RaycastHit hitInfo);-> Castea un ray, desde el punto origen "origin", en la direcci�n 
         *                                                     "direction", de longitud "maxDistance", a todos los colliders de la escena.
         * RaycastHit.collider.------------------------------> Collider que recibio el hit.
         *                     CompareTag(string)------------>  Comprueba la etiqueta (tag) con la etiqueta pasada.
         */
        public GameObject IsTapOver3DModel(Vector2 touchPosition)
        {
            Ray ray = aRCamera.ScreenPointToRay(touchPosition);
            if (Physics.Raycast(ray, out RaycastHit hit3DModel))
            {
                if (hit3DModel.collider.CompareTag("Item"))
                {
                    return hit3DModel.transform.parent.gameObject;
                }
            }
            return null;
        }

        /* isTapOverUI :: M�todo para detectar si el toque en pantalla (touch) est� tocando alg�n elemento de la UI.
         * EventSystem.current-------------> Devuelve el EventSystem que este siendo usado en dicho momento.
         * PointerEventData----------------> Eventos asociados con los puntero (rat�n/t�ctil).
         *                 .position-------> Posici�n actual del pointer actual.
         * EventSystem.current.RaycastAll--> Realiza un Raycast en la Scene usando todos los BaseRaycasters configurados.
         *           (PointerEventData a,----> Datos actuales del puntero.
         *            List<RaycastResult> b);------> Lista de 'hits'.
         */

        public bool IsTapOverUI(Vector2 touchPosition)
        {
            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = new Vector2(touchPosition.x, touchPosition.y);

            List<RaycastResult> result = new();
            EventSystem.current.RaycastAll(eventData, result);

            return result.Count > 0;
        }
    }
}