using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using ArFunctions;
using DG.Tweening;

public class ARInteractionsManager : MonoBehaviour
{

    private GameObject aRPointer;               // Puntero
    private GameObject itemOptionsPanel;        // Panel de opciones 
    private FurnitureItem itemSelected;         // Objeto seleccionado
    
    // Flags que controlan en que modo esta el objeto.
    private bool isInitialPosition;              // flag: �Ha sido colocado el objeto ya en forma preview?
    private bool isMoving;                       // Modo mover
    private bool isRotating;                     // Modo Rotar.
    private bool isScaling;                      // Modo Escalar.

    // Par�metros utilizados para las distintas funciones
    private Vector2 pointScreen;                 // Punto en la mitad de la pantalla.
    private Vector2 initialTouchPos;             // Posicion del toque inicial. Lo usamos para medir el movimiento del toque.
    private float initialDistanceBetweenTouchs;  // Distancia inicial entre los toques. Lo usamos para calcular la distancia movida

    [SerializeField] Camera aRCamera;            // C�mara que empleamos
    private float heightPortion;
    private bool isStaticRotated = false;
    [SerializeField] GameObject ARPositionCanvas;
    private GameObject approveFurnitureButton;
    private ARPlaneManager arPlaneManager;
    private bool itemTouchManagementOn = false;
    // Factores que influencian en la rotaci�n y el escalado de modelo.
    private float rotationFactor = 3f;
    private float rotationTolerance = 1.5f;
    private float scaleFactor = 0.6f;
    private float scaleTolerance = 25.0f;

    //--Objetos con todos los m�todos que nos pueden hacer falta.
    private RayCastManager rayCastManager;
    private touch hitChecker;
    private AROcclusionManager arOcclusionManager;
    private bool DepthMapOn = false;
    //-ItemDescriptionWindow
    [SerializeField] public GameObject itemDescriptionWindow;
    [SerializeField] public GameObject infoButton;
    [SerializeField] private GameObject arLight;
    private bool windowOpened = false;
    //- Helper
    [SerializeField] private UIManager uiManager;
    //Anchor
    private ARAnchorManager anchorManager;

    public FurnitureItem ItemSelected { 
        set
        {
            itemSelected = value;
            ItemPositioningOn(); //Quiz�s as� lo llama todas las veces, yo k se XD
            //oldParent = value.transform.parent;
        }
    }

    void Start()
    {
        aRPointer = transform.GetChild(0).gameObject;                   // Asignamos nuestro puntero.
        
        itemOptionsPanel = transform.GetChild(1).gameObject;            // Asignamos nuestro panel de opciones de objeto.
        pointScreen = new Vector2(Screen.width/2, Screen.height/2);     // Tomamos el punto intermedio de la pantalla. !!! Tener en cuenta que eal rotar la pantalla, las coordenadas cambian tambi�n.
        arOcclusionManager = aRCamera.GetComponent<AROcclusionManager>();
        itemSelected = null;
        //deleteFurnitureButton = ARPositionCanvas.transform.GetChild(0).gameObject;
        approveFurnitureButton = ARPositionCanvas.transform.GetChild(1).gameObject;
        arPlaneManager = FindObjectOfType<ARPlaneManager>();
        anchorManager = FindObjectOfType<ARAnchorManager>();

        // P�rametros que definen en que estado se encuentra el objeto. 
        isRotating = false;     // �Est� rotando el modelo?
        isMoving = false;       // �Se est� moviendo?
        isScaling = false;      // �Su tama�o esta siendo reescalado?

        // ARFunctions-RayCastManager - Obtenemos los objetos a trav�s de los que modificaremos par�metros de las operaciones realizadas aqu� (CastManager)
        // Creamos los objetos de los que obtendremos las funciones 
        rayCastManager = new ArFunctions.RayCastManager(FindObjectOfType<ARRaycastManager>()); // ARFunctions-RayCastManager
        hitChecker = new ArFunctions.touch(aRCamera);                                          // ARFunctions-touch.     
    }

    // Comprobaciones que hacemos cada frame
    void Update() 
    {
        if (Application.isEditor && isInitialPosition)
        {
            GetHitPlaneEditor();
        }
        else 
        if (Application.isPlaying && isInitialPosition)
        {
            ItemPositioning();
        }
        else if (itemTouchManagementOn)
        {
            ItemTouchManagement();
        }
    }

    private void GetHitPlaneEditor()
    {
        // Bit shift the index of the layer (8) to get a bit mask
        // 8  -> floor
        // 9  -> wall
        // 10 -> ceiling
        Color colorRay = Color.black;
        int layerMask;
        switch (itemSelected.PlaneType) // ha detectado, problablemente unos cuantos, hora de filtrar.
        {
            case "floor":
                layerMask = 1 << 8;
                colorRay = Color.yellow;
                break;
            case "wall":
                layerMask = 1 << 9;
                colorRay = Color.blue;
                break;
            case "ceiling":
                colorRay = Color.green;
                layerMask = 1 << 10;
                break;
            default:
                layerMask = 1 << 1;
                break;
        }
        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        //layerMask = ~layerMask;

        RaycastHit hit;
        // Does the ray intersect layer x (8).
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward) * hit.distance, colorRay);
            approveFurnitureButton.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
            aRPointer.transform.position = hit.point + new Vector3(0, 0.01f, 0);

            Quaternion rotation;
            switch (itemSelected.PlaneType)
            {
                case "ceiling":
                    rotation = Quaternion.LookRotation(hit.transform.forward) * Quaternion.Euler(-90, 0, 0);
                    aRPointer.transform.position -= new Vector3(0, 0.05f, 0);
                    break;
                case "floor":
                    rotation = Quaternion.LookRotation(hit.transform.forward) * Quaternion.Euler(90, -90, -90);
                    aRPointer.transform.position += new Vector3(0, 0.05f, 0);
                    break;
                case "wall":
                    rotation = Quaternion.LookRotation(hit.transform.up) * Quaternion.Euler(180, 0, 180);
                    aRPointer.transform.position -= new Vector3(0, 0, 0.05f);
                    break;
                default:
                    rotation = Quaternion.LookRotation(hit.transform.forward);
                    break;
            }
            aRPointer.transform.rotation = rotation;
        }
        else
        {
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            approveFurnitureButton.transform.DOScale(new Vector3(0, 0, 0), 0.3f);
            Ray ray = aRCamera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / heightPortion));
            aRPointer.transform.position = ray.GetPoint(2);
            aRPointer.transform.Rotate(0, 0, 70 * Time.deltaTime);
        }

    }

    // Se emplea para controlar la colocaci�n automatica del objeto cuando estamos en ARPosition
    public void ItemPositioningOn()
    {
        //Verificamos que tenemos un objeto seleccionado
        if (itemSelected != null)
        {
            itemTouchManagementOn = false;
            aRPointer.transform.eulerAngles = new Vector3(90, 0, 0); //lo ponemos donde toca 
            isInitialPosition = true;
            aRPointer.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
            itemSelected.Item3DModel.transform.parent = aRPointer.transform;

            RotateStaticArpointer(); 
            
            if (arOcclusionManager && !DepthMapOn) { arOcclusionManager.requestedEnvironmentDepthMode = EnvironmentDepthMode.Disabled; } //Desactivamos arOcclusionManager
            arPlaneManager.requestedDetectionMode = PlaneDetectionMode.Horizontal | PlaneDetectionMode.Vertical;

            switch (itemSelected.PlaneType)
            {
                case "ceiling":
                    heightPortion = 1.5f;
                    break;
                case "floor":
                    heightPortion = 4;
                    break;
                case "wall":
                    heightPortion = 2;
                    break;
                default:
                    break;
            }
        }
    }

    // Nota: Hay que normalizar a euler para pasar a quaterniones
    private void RotateStaticArpointer(){ 
        if (itemSelected != null) 
        {
            switch(itemSelected.PlaneType){
                case "ceiling":
                    aRPointer.transform.eulerAngles = new Vector3(270, 0, 0);
                    break;
                case "floor":
                    aRPointer.transform.eulerAngles = new Vector3(90, 0, 0);
                    break;
                case "wall":
                    aRPointer.transform.eulerAngles = new Vector3(0, 0, 0);
                    break;
                default:
                    break;
            }
        }
    }

    // Funci�n que coloca el modelo por primera vez en la pantalla
    private void ItemPositioning()
    {
        var planeHitted = rayCastManager.CastRay(pointScreen, itemSelected.PlaneType); // Buscamos planos en mitad de la pantalla, el resultado se devuelve en hits.
        if (arOcclusionManager && !DepthMapOn) { arOcclusionManager.requestedEnvironmentDepthMode = EnvironmentDepthMode.Disabled; }

        if (planeHitted)        // Detectamos al menos un hit (plano) correcto. As� que colocamos el objeto ah�.
        {
            approveFurnitureButton.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
            var planeHit = arPlaneManager.GetPlane(rayCastManager.BestHit.trackableId);

            aRPointer.transform.position = rayCastManager.BestHit.pose.position; 
            Quaternion rotation;
            switch (itemSelected.PlaneType)
            {
                case "ceiling":
                    rotation = Quaternion.LookRotation(planeHit.transform.forward) * Quaternion.Euler(-90, 0, 0);
                    aRPointer.transform.position -= new Vector3(0, 0.05f, 0);
                    break;
                case "floor":
                    rotation = Quaternion.LookRotation(planeHit.transform.forward) * Quaternion.Euler(90, -90, -90);
                    aRPointer.transform.position += new Vector3(0, 0.05f, 0);
                    break;
                case "wall":
                    rotation = Quaternion.LookRotation(planeHit.transform.up) *  Quaternion.Euler(180, 0, 180);
                    aRPointer.transform.position -= new Vector3(0, 0, 0.05f);
                    break;
                default:
                    rotation = Quaternion.LookRotation(planeHit.transform.forward);
                    break;
            }
            aRPointer.transform.rotation = rotation;

            uiManager.helperFirstTimeEnd();
            if (!aRPointer.activeSelf)
            {
                aRPointer.SetActive(true);
            }
        }
        else
        {
            approveFurnitureButton.transform.DOScale(new Vector3(0, 0, 0), 0.3f);
            uiManager.helperFirstTime();
        }
    }

    public void ItemPositioned()
    {
        if (isInitialPosition)
        {
            if (arOcclusionManager && !DepthMapOn) { arOcclusionManager.requestedEnvironmentDepthMode = EnvironmentDepthMode.Best; }
            itemOptionsPanel.SetActive(true);
            uiManager.HelperType = 5;
            if ((uiManager.HelperFirstTimeFlag & 1) != 0) // 0 0 0 0 0 1 
            {
                uiManager.helperCalls();
                uiManager.HelperFirstTimeFlag = uiManager.HelperFirstTimeFlag & 62; // 1 1 1 1 1 0 = 62
            }
            isInitialPosition = false;
            itemSelected.OriginalScale = itemSelected.item3DModel.transform.localScale; // Cuando se coloca por primera vez, almacenamos su escala.
            itemTouchManagementOn = true;
            arPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
        }
        else
        {
            ItemDiselected();
        }
    }

    public void ItemDiselected()
    {
        if (itemSelected != null)
        {
            aRPointer.SetActive(false);
            itemSelected.Item3DModel.transform.parent = itemSelected.transform;
            itemOptionsPanel.SetActive(false);
            uiManager.PreviousHelperType();
            itemSelected = null;
            GameManager.instance.MainMenu();
        }
    }

    // Funci�n para gestionar las distintas funciones que manejan los modelos (mediante gestos con los dedos)
    private void OneTouchManagement(Touch touchOne)
    {
        if (touchOne.phase == TouchPhase.Began)
        {
            isMoving = false;
            if (hitChecker.IsTapOver3DModel(touchOne.position) && !hitChecker.IsTapOverUI(touchOne.position))
            {
                isMoving = true;
                initialTouchPos = touchOne.position;
                isInitialPosition = false;              
            }
        }
        else if (touchOne.phase == TouchPhase.Moved)
        {
            if (rayCastManager.CastRay(touchOne.position, itemSelected.PlaneType) && isMoving)
            {
                Pose hitPose = rayCastManager.BestHit.pose;
                if (!hitChecker.IsTapOverUI(touchOne.position))
                {
                    aRPointer.transform.position = hitPose.position;          
                    initialTouchPos = aRPointer.transform.position;
                }
            }
        }
    }

    private void TwoTouchesManagement(Touch touchOne, Touch touchTwo)
    {
        if (touchOne.phase == TouchPhase.Began || touchTwo.phase == TouchPhase.Began)
        {
            if (!hitChecker.IsTapOverUI(touchOne.position) && !hitChecker.IsTapOverUI(touchTwo.position))
            {
                isRotating = false; 
                isScaling = false; 
                if (hitChecker.IsTapOver3DModel(touchOne.position) && hitChecker.IsTapOver3DModel(touchTwo.position))
                {
                    initialDistanceBetweenTouchs = Mathf.Abs(Vector2.Distance(touchOne.position, touchTwo.position)); 
                    isScaling = true;
                }
                else if (!hitChecker.IsTapOver3DModel(touchTwo.position) || !hitChecker.IsTapOver3DModel(touchOne.position))
                {
                    initialTouchPos = touchTwo.position - touchOne.position; 
                    isRotating = true;
                }
            }
        }
        else if (touchOne.phase == TouchPhase.Moved || touchTwo.phase == TouchPhase.Moved)
        {
            if (isScaling) 
            {
                float currentDistanceBetweenTouchs = Mathf.Abs(Vector2.Distance(touchOne.position, touchTwo.position));
                float diffDis = currentDistanceBetweenTouchs - initialDistanceBetweenTouchs;
                if (Mathf.Abs(diffDis) > scaleTolerance && itemSelected)
                {
                    Vector3 newScale = itemSelected.Item3DModel.transform.localScale + (Mathf.Sign(diffDis) * scaleFactor * Vector3.one);
                    itemSelected.Item3DModel.transform.localScale = newScale;
                    initialDistanceBetweenTouchs = currentDistanceBetweenTouchs;
                }
            }
            else if (isRotating) 
            {
                Vector2 currentTouchPos = touchTwo.position - touchOne.position;
                float angle = Vector2.SignedAngle(initialTouchPos, currentTouchPos);    
                if (Mathf.Abs(angle) > rotationTolerance && itemSelected)
                {
                    itemSelected.Item3DModel.transform.rotation 
                        = Quaternion.Euler(0, itemSelected.Item3DModel.transform.eulerAngles.y - Mathf.Sign(angle) * rotationFactor, 0);   
                }
                initialTouchPos = currentTouchPos; 
            }
        }
    }

    /* -- Si tenemos el modelo seleccionado --
    *   el tap es sobre un modelo 3D / ning�n modelo 3D ya asignando / tap no ha sido en la interfaz
    *
    * -- Si no tenemos el modelo seleccionado --
    *   No hay ning�n modelo 3D asignado as� que comprobamos si el usuario ha clicado sobre uno para seleccioinarlo
    *   Ning�n modelo 3D asignado / tap es sobre un modelo 3D / no ha sido sobre la interfaz
    */

    private void ItemTouchManagement()
    {
        if (Input.touchCount > 0)
        {
            Touch touchOne = Input.GetTouch(0); 
            
            if (itemSelected != null) 
            {
                switch (Input.touchCount)
                {
                    case 1:
                        OneTouchManagement(touchOne);
                        break;
                    case 2:
                        TwoTouchesManagement(touchOne, Input.GetTouch(1));
                        break;
                    default:
                        Debug.Log("Error ocurrido");
                        break;
                }
            }
            else if (!hitChecker.IsTapOverUI(touchOne.position)) 
            {
                var gameObject = hitChecker.IsTapOver3DModel(touchOne.position);
                if (gameObject)
                {
                    itemSelected = gameObject.GetComponentInParent<FurnitureItem>();    
                    GameManager.instance.ARPosition();                           
                    aRPointer.SetActive(true);                                   
                    itemOptionsPanel.SetActive(true);                            
                    uiManager.HelperType = 5;
                    itemSelected.Item3DModel.transform.parent = aRPointer.transform;
                }
            }
        }
    }

    //--- Funciones Externas para botones  --------------
    //--Funciones del modelo
    public void DeleteItem()
    {
        if (itemSelected != null)
        {
            aRPointer.SetActive(false);
            itemOptionsPanel.SetActive(false);
            uiManager.PreviousHelperType();
            itemSelected.Item3DModel.transform.parent = itemSelected.transform;
            isInitialPosition = false; // OJO!

            itemSelected.DeleteItem();
            itemSelected = null;
            uiManager.helperFirstTimeEnd();
        }
    }

    public void ResetScale()
    {
        if (itemSelected != null)
        {
            itemSelected.ResetScale();
        }
    }

    public void ChangeMaterial()
    {
        if (itemSelected)
        {
            itemSelected.ChangeMaterial();
        }
    }

    public void FurnitureLightSwitch()
    {
        if (arLight.activeSelf)
        {
            arLight.SetActive(false);
        }
        else
        {
            arLight.SetActive(true);
        }
    }

    // ** Funciones de ItemDescriptionWindow  **
    // * Mostrar Ventana con la descripci�n
    public void ShowLongDescription()
    {
        if (!windowOpened && itemSelected != null && itemSelected.GetComponent<FurnitureItem>() != null)
        {
            //Cambiamos el t�tulo y la descripci�n seg�n el item seleccionado.
            itemDescriptionWindow.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = itemSelected.GetComponent<FurnitureItem>().ItemName;
            itemDescriptionWindow.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = itemSelected.GetComponent<FurnitureItem>().ItemLongDescription;

            // Movimiento de la ventana
            itemDescriptionWindow.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
            itemDescriptionWindow.transform.DOMoveX(Screen.width / 2.0f, 0.3f);
            windowOpened = true;
        }
        else
        {
            CloseLongDescriptionWindow();
        }
    }
    // * Cerrar ventana con la descripci�n
    public void CloseLongDescriptionWindow()
    {
        // Movimiento de la ventana
        itemDescriptionWindow.transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        itemDescriptionWindow.transform.DOMoveX(infoButton.transform.position.x, 0.3f);
        windowOpened = false;
    }
    public void DepthMapSwitch(bool value)
    {
        DepthMapOn = value;
    }
}
