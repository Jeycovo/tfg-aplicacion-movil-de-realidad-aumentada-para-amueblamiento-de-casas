using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FondoAnimado : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var par = false;
        foreach (Transform child in transform)
        {
            if (!par)
            {
                child.DOMoveY(child.position.y + 5, 3.0f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
                par = true;
            }
            else
            {
                child.DOMoveY(child.position.y - 5, 3.0f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
                par = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
