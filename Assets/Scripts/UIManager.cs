using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using TMPro;
using DG.Tweening; //DOTween (HOTween v2) -> DOTween is a fast, efficient, fully type-safe object-oriented animation engine
using System;
using UnityEngine.XR.ARSubsystems;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject mainMenuCanvas;
    [SerializeField] private GameObject itemsMenuCanvas;
    [SerializeField] private GameObject ARPositionCanvas;
    [SerializeField] private GameObject messageCanvas;
    [SerializeField] private GameObject planeCalibrationCanvas;
    [SerializeField] private GameObject furnitureSpace;

    // DeveloperOptions
    [SerializeField] private GameObject developerOptionsPanel;
    [SerializeField] private RawImage DepthImageVisualizer;
    [SerializeField] private GameObject DepthImageVisualizerGameObject;
    [SerializeField] private GameObject ARSessionOrigin;                   //GameObject que contiene el AR Point Cloud Manager
    [SerializeField] private ARInteractionsManager ARInteractionsManager;  //ARIteractionsManager

    // Start Menu
    [SerializeField] private GameObject arLight;
    [SerializeField] private GameObject startMenu;
    [SerializeField] private Canvas startMenuCanvas;
    [SerializeField] private GameObject ARCamera;

    // Helpers
    [SerializeField] private GameObject CanvasHelper;
    [SerializeField] private GameObject ButtonHelper;

    private bool developerOptionsPanelState = false;
    private ARPlaneManager ARPlaneManager;
    private AROcclusionManager AROcclusionManager;
    private bool planeSwitchValue = true;
    private bool depthMapOn = false;
    private int helperFirstTimeFlag = 63; // Flag que representan si la ayuda ha sido mostrada al menos la primera vez || 1 1 1 1 1 1 -> 32 + 16 + 8 + 4 + 2 + 1 = 63 ||
    public int HelperFirstTimeFlag { get => helperFirstTimeFlag; set => helperFirstTimeFlag = value;}

    private void Awake()
    {
        ARPlaneManager = ARSessionOrigin.GetComponent<ARPlaneManager>();
        AROcclusionManager = ARCamera.GetComponent<AROcclusionManager>();
        PlaneSwitch(false);                                                     //Desactivamos la el plane prefab ya que no queremos que se active por defecto.
        helperMessage = CanvasHelper.transform.GetChild(6).gameObject;
        iconTransform = helperMessage.transform.GetChild(1).transform;
        iconTransform.DORotate(new Vector3(0, 30, 0), 2.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnStartMenu += ActivateStartMenu;
        GameManager.instance.OnMainMenu += ActivateMainMenu;
        GameManager.instance.OnItemsMenu += ActivateItemsMenu;
        GameManager.instance.OnARPosition += ActivateARPosition;
        GameManager.instance.OnPlaneCalibration += PlaneCalibration;
    }

    private void ActivateStartMenu()
    {
        ButtonHelper.SetActive(false);
        startMenu.SetActive(true);

        mainMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(new Vector3(0, 0, 0), 0); 
        mainMenuCanvas.transform.GetChild(3).transform.DOScale(new Vector3(0, 0, 0), 0); 

        planeCalibrationCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        ARPlaneManager.requestedDetectionMode = PlaneDetectionMode.None;
        PlaneSwitch(false);

        //planeCalibrationCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);

        if (developerOptionsPanelState) { doorDeveloperPanel(); } //Si el panel de desarrollador esta abierto, lo cerramos.
        if (furnitureSpace.activeSelf) { furnitureSpace.SetActive(false); }
        
    }

    private void ActivateMainMenu()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.3f); 
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(1, 1, 1), 0.3f); 
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(new Vector3(1, 1, 1), 0.3f); 
        mainMenuCanvas.transform.GetChild(3).transform.DOScale(new Vector3(1, 1, 1), 0.3f); 

        itemsMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOMoveY(180, 0.3f);

        ARPositionCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        ARPositionCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);

        if (furnitureSpace.activeSelf) { furnitureSpace.SetActive(true); }
        ButtonHelper.SetActive(true);
        startMenu.SetActive(false);

        HelperType = 0;
        if ((helperFirstTimeFlag & 32) != 0) // 1 0 0 0 0 0 
        {
            helperCalls();
            HelperFirstTimeFlag = HelperFirstTimeFlag & 31; // 0 1 1 1 1 1 = 31
        }
    }


    private void PlaneCalibration()
    {
        startMenu.SetActive(false);
        ButtonHelper.SetActive(true);

        mainMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(3).transform.DOScale(new Vector3(0, 0, 0), 0.3f);

        planeCalibrationCanvas.transform.GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.3f);

        //Activamos cosas de Calibración (Idea, modificar para poder elegir el tipo de horientación.)
        ARPlaneManager.requestedDetectionMode = PlaneDetectionMode.Horizontal | PlaneDetectionMode.Vertical;
        PlaneSwitch(true);
        HelperType = 4;
        if ((helperFirstTimeFlag & 2) != 0) // 0 0 0 0 1 0 
        {
            helperCalls();
            HelperFirstTimeFlag = HelperFirstTimeFlag & 61; // 1 1 1 1 0 1 = 61
        }
    }

    private void ActivateItemsMenu()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(3).transform.DOScale(new Vector3(0, 0, 0), 0.3f); //DeveloperOptions

        if (developerOptionsPanelState) {doorDeveloperPanel();} //Si el panel de desarrollador esta abierto, lo cerramos.

        itemsMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.5f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOMoveY(300, 0.3f);

        ARPositionCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        ARPositionCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);

        HelperType = 1;
        if ((helperFirstTimeFlag & 16) != 0) // 0 1 0 0 0 0 
        {
            helperCalls();
            HelperFirstTimeFlag = HelperFirstTimeFlag & 47; // 1 0 1 1 1 1 = 32 + 0 + 8 + 4 + 2 + 1 = 47
        }
    }

    private void ActivateARPosition()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(new Vector3(0, 0, 0), 0.3f);

        mainMenuCanvas.transform.GetChild(3).transform.DOScale(new Vector3(1, 1, 1), 0.3f); //DeveloperOptions

        itemsMenuCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.5f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        itemsMenuCanvas.transform.GetChild(1).transform.DOMoveY(180, 0.3f);

        ARPositionCanvas.transform.GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        ARPositionCanvas.transform.GetChild(1).transform.DOScale(new Vector3(1, 1, 1), 0.3f);

        HelperType = 2;
        if ((helperFirstTimeFlag & 8) != 0) //  0 0 1 0 0 0 
        {
            helperCalls();
            helperARactive = true;
            HelperFirstTimeFlag &= 55; // 1 1 0 1 1 1 = 55
        }
    }

    

    public void ErrorMessage(string mensaje)
    {
        messageCanvas.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = mensaje;
        messageCanvas.transform.GetChild(0).transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        StartCoroutine(minimizingErrorMessage());
    }

    IEnumerator minimizingErrorMessage()
    {
        yield return new WaitForSeconds(5);
        messageCanvas.transform.GetChild(0).transform.DOScale(new Vector3(0, 0, 0), 0.3f); //Queremos que desaparezca tras 5 segundos.
    }

    // ** Funciones para controlar las opciones de desarrollador **
    // * Función para abrir/cerrar el panel con las opciones de desarrollo.

    public void doorDeveloperPanel()
    {
        GameObject devPanelButton = mainMenuCanvas.transform.GetChild(3).gameObject;
        if (!developerOptionsPanelState)
        {
            developerOptionsPanel.transform.DOMoveY(developerOptionsPanel.transform.position.y - 375, 0.5f);
            devPanelButton.transform.DOMoveY(devPanelButton.transform.position.y + 375, 0.5f);
            developerOptionsPanelState = true;
            HelperType = 3;
            if ((helperFirstTimeFlag & 4) != 0) // 0 0 0 0 1 0 0 
            {
                helperCalls();
                HelperFirstTimeFlag = HelperFirstTimeFlag & 59; // 1 1 1 0 1 1 = 59
            }
        }
        else if (!depthMapOn)
        {
            developerOptionsPanel.transform.DOMoveY(developerOptionsPanel.transform.position.y + 375, 0.5f);
            devPanelButton.transform.DOMoveY(devPanelButton.transform.position.y - 375, 0.5f);
            developerOptionsPanelState = false;
            PreviousHelperType();
        }
    }

    public void PreviousHelperType()
    {
        HelperType = previousHelperType;
    }

    public void DeveloperOptionsManager(int value)
    {
        switch (value)
        {
            case 0: //DepthMapVisualizer
                Debug.Log("Developer Option: Depth Map");
                if (!DepthImageVisualizerGameObject.activeSelf) {
                    DepthImageVisualizerGameObject.SetActive(true);
                    ARInteractionsManager.DepthMapSwitch(true);
                    depthMapOn = true;
                    AROcclusionManager.requestedEnvironmentDepthMode = UnityEngine.XR.ARSubsystems.EnvironmentDepthMode.Fastest;
                    developerOptionsPanel.transform.GetChild(0).GetChild(0).transform.DOMoveX(Screen.width*0.5f, 0.5f); //DepthMap button
                    developerOptionsPanel.transform.GetChild(0).GetChild(1).transform.DOScale(new Vector3(0,0,0), 0.5f); //Plane Button
                    developerOptionsPanel.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(false); //Texto
                    developerOptionsPanel.GetComponent<Image>().CrossFadeAlpha(255, 1f, true);
                } 
                else 
                {
                    AROcclusionManager.requestedEnvironmentDepthMode = UnityEngine.XR.ARSubsystems.EnvironmentDepthMode.Disabled;
                    ARInteractionsManager.DepthMapSwitch(false);
                    DepthImageVisualizerGameObject.SetActive(false);
                    depthMapOn = false;
                    developerOptionsPanel.transform.GetChild(0).GetChild(0).transform.DOMoveX(230, 0.5f); //DepthMap button
                    developerOptionsPanel.transform.GetChild(0).GetChild(1).transform.DOScale(new Vector3(1, 1, 1), 0.5f); //Plane Button
                    developerOptionsPanel.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(true); //Texto
                    developerOptionsPanel.GetComponent<Image>().CrossFadeAlpha(150, 1f, true);
                }
                break;
            case 1: //AR Plane Manager
                Debug.Log("Developer Option: Ar Plane Manager");
                if (planeSwitchValue) { PlaneSwitch(false); } else { PlaneSwitch(true); }
                break;
            default:
                break;
        }
    }

    private void PlaneSwitch(bool value)
    {
        Debug.Log("PlaneSwitch: " + value);
        if (value != planeSwitchValue)
        {
            ARPlaneManager.planePrefab.SetActive(value);
            foreach (var plane in ARPlaneManager.trackables)
            {
                plane.GetComponent<MeshRenderer>().enabled = value;
                plane.GetComponent<ARPlaneMeshVisualizer>().enabled = value;
            }
            planeSwitchValue = value;
        }
    }

    public void menuCalls(int type)
    {
        switch (type)
        {
            case 0:
                GameManager.instance.MainMenu();
                break;
            case 1:
                GameManager.instance.CloseApp();
                break;
            case 2:
                GameManager.instance.PlaneCalibration();
                break;
            default:
                Debug.LogWarning("Opcion no admitida");
                break;
        }
    }

    // Par de metodos usados para mostrar la animación del helper la primera vez.
    private GameObject helperMessage;
    private Transform iconTransform;
    private bool active = false;
    private bool helperARactive = false;

    public void helperFirstTime()
    {
        if (!active && !helperARactive)
        {
            active = true;
            helperMessage.SetActive(true);
            //iconTransform.DORotate(new Vector3(0, 0, iconTransform.rotation.eulerAngles.z+45), 2.5f).SetLoops(1, LoopType.Yoyo).SetEase(Ease.Linear).OnComplete(helperFirstTimeEnd);
        }
        
    }

    public void helperFirstTimeEnd()
    {
        //iconTransform.rotation.eulerAngles = new Vector3(0, 0, -20);
        active = false;
        helperMessage.SetActive(false);
        
    }
    public void helperEndedARMessage()
    {
        helperARactive = false;
        helperExit();
    }

    private int helperType = -1;
    private int previousHelperType = -1;
    public int HelperType { 
        set
        {
            previousHelperType = helperType;
            helperType = value;
        }   
     }

    private GameObject helperActivated;  
    private Transform messageActivated;

    public void helperCalls()
    {
        helperActivated = CanvasHelper.transform.GetChild(helperType).gameObject;
        messageActivated = helperActivated.transform.GetChild(0).GetChild(0);

        helperActivated.SetActive(true);
        messageActivated.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 3.0f).SetLoops(-1, LoopType.Yoyo);
    }

    //Salir del helper
    public void helperExit()
    {
        helperActivated.SetActive(false);
        messageActivated.DOKill();
        helperActivated = null;
    }
}