using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ChairMenuRotation : MonoBehaviour
{

    // Start is called before the first frame update
    [SerializeField] Camera aRCamera;
    void Start()
    {
        chairAnimation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void chairAnimation()
    {
        var time = 0.05f;
        Ray ray = aRCamera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height * 0.5f));
        var position = ray.GetPoint(2.75f);
        var rotation = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + 0.75f);
        transform.DORotate(rotation, time).SetEase(Ease.Linear);
        transform.DOMove(position, time).SetEase(Ease.Linear).OnComplete(chairAnimation);
        
        //transform.Rotate(rotation);
    }
}
