# TFG - Aplicación móvil de realidad aumentada para amueblamiento de casas
## Introducción
El proyecto consiste en una investigación del estado del arte de la Realidad Aumentada, así como de las diversas tecnologías que permiten su implementación. Posteriormente centramos el estudio en una de las tecnologías disponibles, Unity, la cúal empleamos para desarrollar nuestra propia app de RA. Concretando, esta app permitirá tomar las medidas de la habitación en la que nos encontremos y se valdrá del paradigma de la realidad aumentada, empleando las referencias capturadas con la cámara, para permitirnos colocar guíado por nosotros, el usuario, muebles virtuales seleccionados de un catálogo determinado. Estos muebles podrán ser movidos y rotados de su posición, dando al usuario la oportunidad de hacerse una idea clara de las medidas del mueble y como queda.

## Autor
Jacobo Placeres Cabrera

## Nombre del proyecto
Aplicación móvil de realidad aumentada para amueblamiento de casas.

## Tecnologías
ARCore, Unity, Visual Studio...

## Guía de uso
Los scripts con el código se encuentra en Assets/Scripts
